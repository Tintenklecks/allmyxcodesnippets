<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>IDECodeSnippetCompletionPrefix</key>
	<string>swift-ac-binarySearch</string>
	<key>IDECodeSnippetCompletionScopes</key>
	<array>
		<string>TopLevel</string>
	</array>
	<key>IDECodeSnippetContents</key>
	<string>/**
	 Binary Search
	
	 Recursively splits the array in half until the value is found.
	
	 If there is more than one occurrence of the search key in the array, then
	 there is no guarantee which one it finds.
	
	 Note: The array must be sorted!
	 **/
	
	import Foundation
	
	// The recursive version of binary search.
	public func binarySearch&lt;T: Comparable&gt;(_ a: [T], key: T, range: Range&lt;Int&gt;) -&gt; Int? {
	    if range.lowerBound &gt;= range.upperBound {
	        return nil
	    } else {
	        let midIndex = range.lowerBound + (range.upperBound - range.lowerBound) / 2
	        if a[midIndex] &gt; key {
	            return binarySearch(a, key: key, range: range.lowerBound ..&lt; midIndex)
	        } else if a[midIndex] &lt; key {
	            return binarySearch(a, key: key, range: midIndex + 1 ..&lt; range.upperBound)
	        } else {
	            return midIndex
	        }
	    }
	}
	
	/**
	 The iterative version of binary search.
	
	 Notice how similar these functions are. The difference is that this one
	 uses a while loop, while the other calls itself recursively.
	 **/
	
	public func binarySearch&lt;T: Comparable&gt;(_ a: [T], key: T) -&gt; Int? {
	    var lowerBound = 0
	    var upperBound = a.count
	    while lowerBound &lt; upperBound {
	        let midIndex = lowerBound + (upperBound - lowerBound) / 2
	        if a[midIndex] == key {
	            return midIndex
	        } else if a[midIndex] &lt; key {
	            lowerBound = midIndex + 1
	        } else {
	            upperBound = midIndex
	        }
	    }
	    return nil
	}</string>
	<key>IDECodeSnippetIdentifier</key>
	<string>59D77C21-4145-4768-A531-C5A64DC468B6</string>
	<key>IDECodeSnippetLanguage</key>
	<string>Xcode.SourceCodeLanguage.Swift</string>
	<key>IDECodeSnippetSummary</key>
	<string>An implementation of binary search</string>
	<key>IDECodeSnippetTitle</key>
	<string>Swift Algorithm Club Binary Search</string>
	<key>IDECodeSnippetUserSnippet</key>
	<true/>
	<key>IDECodeSnippetVersion</key>
	<integer>2</integer>
</dict>
</plist>
